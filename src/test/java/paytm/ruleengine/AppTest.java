package paytm.ruleengine;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import com.google.gson.JsonObject;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import paytm.application.App;

public class AppTest extends TestCase
{
    private  App app;
    
    public AppTest( String testName) throws JsonParseException, JsonMappingException, IOException
    {
        super(testName);
        String rulesData = IOUtils.toString(
    		      App.class.getResourceAsStream("/newrules"),
    		      "UTF-8"
    		    );
    	app = new App(rulesData);
    }
    
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }
    
    /*
    public void testykwrules() throws IOException{
    	assertTrue(true);
    }*/
    
   /* public void teststresswithsamekey(){
    	for(int i=0;i<5000;i++){
	    	JsonObject fact = new JsonObject();
	    	fact.addProperty("value", i);
	    	int oldh = fact.hashCode();
	    	app.runRules("natural<100", fact);
	    	int newh = fact.hashCode();
        }
    	assertTrue(true);
    }*/
    
    /*public void testApp()
    {
    	for(int i=0;i<5000;i++){
	    	JsonObject fact = new JsonObject();
	    	fact.addProperty("integer", i);
	    	int oldh = fact.hashCode();
	    	app.runRules("natural<100", fact);
	    	int newh = fact.hashCode();
	    	if(i >=1 && i < 100)
	    		assertTrue(oldh != newh);
	    	else
	    		assertTrue( oldh == newh );
        }
    }*/
    
    /*
    public void testApp1(){
    	for(int i=-2500;i<5000;i++){
	    	JsonObject fact = new JsonObject();
	    	fact.addProperty("integer", i);
	    	int oldh = fact.hashCode();
	    	app.runRules("range", fact);
	    	int newh = fact.hashCode();
	    	if(i >=-100 && i <= 0){
	    		if(oldh != newh)
	    			System.out.println(i);
	    		assertTrue(oldh == newh);
	    	}
	    	else{
	    		if(oldh == newh)
	    			System.out.println(i);
	    		assertTrue( oldh != newh );
	    	}
        }
    }*/
    
    //correcness test, to be run with newrules file which has distinct keys
     
    public void testless(){
    	JsonObject fact = new JsonObject();
    	fact.addProperty("integer", 50);
    	int oldh = fact.hashCode();
    	app.runRules("natural<100", fact);
    	int newh = fact.hashCode();
    	assertTrue(oldh != newh);
    }
    
    public void testrange(){
    	JsonObject fact = new JsonObject();
    	fact.addProperty("integerrange", -50);
    	int oldh = fact.hashCode();
    	app.runRules("range", fact);
    	int newh = fact.hashCode();
    	assertTrue(oldh == newh);
    }
    
    public void testset(){
    	JsonObject fact = new JsonObject();
    	fact.addProperty("integerset", 7);
    	int oldh = fact.hashCode();
    	app.runRules("set", fact);
    	int newh = fact.hashCode();
    	assertTrue(oldh != newh);
    }
    
    public void testdatetime(){
    	JsonObject fact = new JsonObject();
    	fact.addProperty("datetime", "2017-06-01 12:14:05");
    	int oldh = fact.hashCode();
    	app.runRules("datetimerange", fact);
    	int newh = fact.hashCode();
    	assertTrue(oldh != newh);
    }
    
    public void testtime(){
    	JsonObject fact = new JsonObject();
    	fact.addProperty("time", "02:14:05");
    	int oldh = fact.hashCode();
    	app.runRules("timerange", fact);
    	int newh = fact.hashCode();
    	assertTrue(oldh != newh);
    }
    
    public void teststringrange(){
    	JsonObject fact = new JsonObject();
    	fact.addProperty("string", "BCMC");
    	int oldh = fact.hashCode();
    	app.runRules("stringrange", fact);
    	int newh = fact.hashCode();
    	assertTrue(oldh != newh);
    }
    
    public void testregex(){
    	JsonObject fact = new JsonObject();
    	fact.addProperty("string", "ab");
    	int oldh = fact.hashCode();
    	app.runRules("regex", fact);
    	int newh = fact.hashCode();
    	assertTrue(oldh != newh);
    }
}
