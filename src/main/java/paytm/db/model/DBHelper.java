package paytm.db.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;


public class DBHelper {
	static{
		Base.open("com.mysql.jdbc.Driver","jdbc:mysql://127.0.0.1:3306/test","root","root");
	}
	
	public static paytm.rule.model.RuleModel[] getRuleModels(){
		
		LazyList<RuleTag> ruletags = RuleTag.findAll();
		
		Set<String> distinctTags = new HashSet<>();
		ruletags.forEach(ruletag -> {
			String tag = ruletag.getString("ruletag_id");
			distinctTags.add(tag);
		});
		
		Map<  paytm.rule.model.RuleModel, Set<String> > ruleToTagsMap = new HashMap<>(); 
		
		distinctTags
				.forEach(tag -> {
					
					paytm.rule.model.RuleModel[] rules = getRulesFromTag(tag);
					
					Arrays.asList(rules).stream().forEach(rule -> {
						if(!ruleToTagsMap.containsKey(rule)){
							ruleToTagsMap.put(rule, new HashSet<String>());
						}
						Set<String> tags = ruleToTagsMap.get(rule);
						tags.addAll(Arrays.asList(rule.tags));
						ruleToTagsMap.put(rule,tags);
					});
				
				});

		List<paytm.rule.model.RuleModel> rulemodels = new ArrayList<>();
		ruleToTagsMap.forEach((rule,tags) -> {
			paytm.rule.model.RuleModel editedRule = new paytm.rule.model.RuleModel(rule.id, rule.name, 
					rule.externalReference, rule.conditionsOperator, rule.priority, 
					tags.toArray(new String[tags.size()]), rule.conditions, rule.actions);
			rulemodels.add(editedRule);
		});	
		
		return rulemodels.toArray(new paytm.rule.model.RuleModel[rulemodels.size()]);
	}
	
	private static paytm.rule.model.RuleModel[] getRulesFromTag(String tag){
		
		List<paytm.rule.model.RuleModel> rlist = new ArrayList<>();
		LazyList<RuleTag> ruletags = RuleTag.where("ruletag_id = ?", tag);

		List<Model>rulemodellist = ruletags.stream()
				.map( ruletag ->  ruletag.getInteger("rule_id"))
				.map(ruleid -> RuleModel.where("id = ?", ruleid))
				.map(list -> list.get(0))
				.filter(rulemodel -> rulemodel.getBoolean("status")!=false)
				.collect(Collectors.toList());

		rulemodellist.forEach(rulemodel -> {
			
			int ruleid = rulemodel.getInteger("id");
			String name = rulemodel.getString("name");
			String external_reference = rulemodel.getString("external_reference");
			int priority = rulemodel.getInteger("priority");
			String conditions_op = rulemodel.getString("conditions_operator");
			
			paytm.rule.model.ConditionModel[] conditions = getConditionModel(ruleid);
			paytm.rule.model.ActionModel[] actions = getActionModel(ruleid);
			
			
			rlist.add(new paytm.rule.model.RuleModel(ruleid, name, external_reference, 
					conditions_op, priority, new String[]{tag} , conditions, actions));
			
		});
		
		return rlist.toArray(new paytm.rule.model.RuleModel[rlist.size()]);
	}
	
	private static paytm.rule.model.ConditionModel[] getConditionModel(int ruleid){
		List<paytm.rule.model.ConditionModel> clist = new ArrayList<>();
		
		ConditionModel.where("rule_id = ?", ruleid)
					.stream().forEach(cmodel -> {
						int cid = cmodel.getInteger("id");
						String type = cmodel.getString("condition");
						String op = cmodel.getString("operation");
						String key = cmodel.getString("key");
						String value = cmodel.getString("value");
						clist.add( new paytm.rule.model.ConditionModel(cid, key, op, value) );
					});
		return  clist.toArray(new paytm.rule.model.ConditionModel[clist.size()]);
	}
	
	private static paytm.rule.model.ActionModel[] getActionModel(int ruleid){
		List<paytm.rule.model.ActionModel> alist = new ArrayList<>();

		ActionModel.where("rule_id = ?", ruleid)
			.stream().forEach(amodel -> {
				
				String key = amodel.getString("key");
				String value = amodel.getString("value");
				String action = amodel.getString("action");
				int id = amodel.getInteger("id");
				alist.add(new paytm.rule.model.ActionModel(id, key, action, value));
			});
		return alist.toArray(new paytm.rule.model.ActionModel[alist.size()]);
	}
}
