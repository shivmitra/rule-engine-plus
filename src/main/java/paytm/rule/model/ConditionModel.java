package paytm.rule.model;

public class ConditionModel {
	public int id;
	public String value;
	public String key;
	public String operation;
	
	public ConditionModel(int id, String key, String op, String value){
		this.id = id;
		this.key = key;
		this.operation = op;
		this.value = value;
	}
	
	public boolean isIndexable(){
		if("set".equals(this.operation))
			return true;
		return false;
	}
	
	public String[] getIndexedValues(){
		if(!isIndexable())
			throw new RuntimeException("not indexable");
		return value.split(",");
	}
	
	@Override
	public boolean equals(Object o){
		if(! (o instanceof ConditionModel)) return false;
		ConditionModel other = (ConditionModel) o;
		if(other.id == this.id)
			return true;
		 
		return false;
	}
	
	@Override
	public int hashCode(){
		return new Integer(this.id).hashCode();
	}
}
