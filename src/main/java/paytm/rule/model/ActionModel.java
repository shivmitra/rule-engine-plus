package paytm.rule.model;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class ActionModel {
	public int id;
	public String action;
	public String value;
	public String key;
	
	//dummy constructor for jackson
	public ActionModel(){}
	
	public ActionModel(int id, String key, String action, String value){
		this.id = id;
		this.action = action;
		this.value = value;
		this.key = key;
	}
}
