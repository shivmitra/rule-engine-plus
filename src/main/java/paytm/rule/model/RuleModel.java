package paytm.rule.model;

import java.util.Arrays;

public class RuleModel {	
	public int id;
	public String name;
	public String externalReference;
	public String conditionsOperator;
	public int priority;
	public String[] tags;
	public ConditionModel[] conditions;
	public ActionModel[] actions;
		
	public RuleModel(int id, String name, String external_reference, String conditionsOperator,
			int priority, String[] tags, ConditionModel[] conditions, ActionModel[] actions){
		this.id = id;
		this.name = name;
		this.externalReference = external_reference;
		this.conditionsOperator = conditionsOperator;
		this.priority = priority;
		this.tags = tags;
		this.conditions = conditions;
		this.actions = actions;
	}
	
	public boolean isIndexable(){
		// if one of the conditions are indexable and conditionsOperator is &&
		if("&&".equals(this.conditionsOperator)){
			return Arrays.stream(this.conditions)
							.map(cond -> cond.isIndexable())
							.reduce(false, (left,current) -> left || current);
		}
		return false;
	}
	
	public String[] getIndexedValues(){
		int idxOfLargestValueArray = -1;
		int largestValueLength = -1;
		
		for(int i=0;i<this.conditions.length;i++){
			if(this.conditions[i].isIndexable() && 
					this.conditions[i].getIndexedValues().length > largestValueLength){
				largestValueLength = this.conditions[i].getIndexedValues().length;
				idxOfLargestValueArray = i;
			}
		}
		
		return this.conditions[idxOfLargestValueArray].getIndexedValues();
	}
	
	
	@Override
	public boolean equals(Object o){
		if(! (o instanceof RuleModel)) return false;
		RuleModel other = (RuleModel) o;
		if(other.id == this.id)
			return true;
		 
		return false;
	}
	
	@Override
	public int hashCode(){
		return new Integer(this.id).hashCode();
	}
}
