package paytm.application;

import java.io.IOException;
import java.io.StringReader;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import paytm.rule.model.RuleModel;

public class Utils {
	
    public static RuleModel[] getRuleModelsFromJson(String rulesData) throws JsonParseException, JsonMappingException, IOException{
    	Gson gson = new Gson();
    	JsonReader reader = new JsonReader(new StringReader(rulesData));
	  	reader.setLenient(true);
        RuleModel[] rulesArray = gson.fromJson(reader, RuleModel[].class);
    	return rulesArray;
    }
    
}
