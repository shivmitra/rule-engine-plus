package paytm.application;
import java.io.IOException;

import org.apache.commons.io.IOUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import paytm.ruleengine.RuleEngine;
import paytm.ruleengine.modelfactory.RuleEngineFactory;

public class DatabaseRulesApplication {
	private RuleEngineFactory rfactory;
	private JsonObject fact;
	
	public DatabaseRulesApplication() throws IOException{
		String data = IOUtils.toString(
	  		      App.class.getResourceAsStream("/requests.txt"),
	  		      "UTF-8"
	  		    );
    	
	  	JsonParser parser = new JsonParser();
	  	JsonElement root = parser.parse(data);
		
	  	fact = root.getAsJsonObject();
	  	rfactory = new RuleEngineFactory(paytm.db.model.DBHelper.getRuleModels());
	}
	
    public void run() throws IOException{
	  	RuleEngine rengine = rfactory.getRuleEngine("tech-cart" ,fact);
	  	System.out.println(fact);
	  	rengine.fireRules();
	  	System.out.println(fact);
    }
}
