package paytm.application;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.google.gson.JsonObject;

import paytm.ruleengine.RuleEngine;
import paytm.ruleengine.modelfactory.RuleEngineFactory;

public class App 
{
	private RuleEngineFactory rfactory;
	
	public App(String rulesData) throws JsonParseException, JsonMappingException, IOException{
		this.rfactory = new RuleEngineFactory(Utils.getRuleModelsFromJson( rulesData ));
	}
	
	public void runRules(String tag, JsonObject fact){
    	RuleEngine rengine = rfactory.getRuleEngine(tag,fact);
    	rengine.fireRules();
	}
	
	public static void prepareForTesting() throws InterruptedException{
		Thread.sleep(20000);	
	}
   
	public static void main( String[] args ) throws InterruptedException, ClassNotFoundException, IOException
    {
    	//prepareForTesting();
    	long start = System.currentTimeMillis();
    	YKWRulesApplication app = new YKWRulesApplication();
    	long startrun = System.currentTimeMillis();
    	for(int i=0;i<10000;i++)
    		app.run();
    	long end = System.currentTimeMillis();
    	
    	System.out.println("Test finished in millisecond - " + (end-start));
    	System.out.println("Test finished in millisecond (without app initialization) - " + (end-startrun));
    }
}
