package paytm.application;
import java.io.IOException;
import java.util.Arrays;

import javax.script.ScriptEngineManager;

import org.apache.commons.io.IOUtils;
import com.google.gson.GsonBuilder;

import paytm.ruleengine.RuleEngine;
import paytm.ruleengine.model.EvaluationAction;
import paytm.ruleengine.modelfactory.RuleEngineFactory;
import paytm.ykw.model.Testcase;

public class YKWRulesApplication {

	 private Testcase[] testArray;
	 private RuleEngineFactory rfactory;
	 
	 public YKWRulesApplication() throws IOException, ClassNotFoundException{
	    	String data = IOUtils.toString(
	  		      App.class.getResourceAsStream("/ykwdata"),
	  		      "UTF-8"
	  		    );
		  	
	    	String rulesData = IOUtils.toString(
				      App.class.getResourceAsStream("/ykwrules"),
				      "UTF-8"
				    );
		  	
		  	rfactory = new RuleEngineFactory(Utils.getRuleModelsFromJson(rulesData));
		  	
		  	testArray = new GsonBuilder().setLenient().create().fromJson(data, Testcase[].class);

		  	EvaluationAction.injectEngine(new ScriptEngineManager().getEngineByName("javascript"));
		  	
	    }
	 	
	    public void run(){
	    	    
	        Arrays.asList(testArray).stream().forEach( test -> {
	        	RuleEngine engine = rfactory.getRuleEngine(test.tag_to_exec, test.message);
	        	/*System.out.println("----------------------------------------------");
	        	System.out.println(test.testCaseName);
	        	System.out.println("input: " + test.message);*/
	        	engine.fireRules();
	        	/*System.out.println("output: " + test.message);
	        	System.out.println("expected: " + test.output);
	        	System.out.println("testresult: " + (test.message.size() == test.output.size()));*/
	        });
	        
	    }
}
