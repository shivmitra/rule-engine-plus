package paytm.ruleengine;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.easyrules.api.RulesEngine;
import org.easyrules.core.RulesEngineBuilder;

import com.google.gson.JsonObject;

import paytm.rule.model.RuleModel;
import paytm.ruleengine.model.Rule;
import paytm.ruleengine.model.interfaces.Action;
import paytm.ruleengine.model.interfaces.Condition;
import paytm.ruleengine.modelfactory.ActionFactory;
import paytm.ruleengine.modelfactory.ConditionFactory;

public class RuleEngine {	
	
	private RulesEngine engine;
	
	public RuleEngine(RuleModel[] rules, JsonObject fact){
		
		engine = RulesEngineBuilder.aNewRulesEngine()
				.build();
		
		Arrays.asList(rules).forEach(rule -> {
			List<Condition> conditions =
					Arrays.asList( rule.conditions ).stream().filter(cond -> Objects.nonNull(cond))
					.map( cond -> 
						ConditionFactory.getCondition("CHECK_VARIABLE",cond.id,cond.key, cond.operation, cond.value))
					.collect(Collectors.toList());
			
			List<Action> actions = 
					Arrays.asList( rule.actions ).stream().filter(action -> Objects.nonNull(action))
					.map(action -> 
						ActionFactory.getAction(action.action, action.key, action.value))
					.collect(Collectors.toList());
			
			Rule engineRule = new Rule(rule.name,conditions,actions,rule.priority,rule.conditionsOperator,fact);
			engine.registerRule(engineRule);
		});
	}
	
	public void setFact(JsonObject fact){
		engine.getRules().forEach(rule -> ((Rule)rule).setFact(fact));
	}
	
	public void fireRules(){
		engine.fireRules();
	}
}
