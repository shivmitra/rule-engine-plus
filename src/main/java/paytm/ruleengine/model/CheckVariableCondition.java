package paytm.ruleengine.model;

import com.google.gson.JsonObject;

import paytm.ruleengine.model.interfaces.Condition;
import paytm.ruleengine.model.interfaces.Operator;

public class CheckVariableCondition implements Condition{
	private int id;
	private String key;
	private Operator op;
	private String value;
	
	public CheckVariableCondition(int id,String key, Operator op, String value){
		this.id = id;
		this.key = key;
		this.op = op;
		this.value = value;
	}
	
	public boolean evaluate(JsonObject fact) {
		String currentValue = new String(value);
		Utils.replaceVariables(fact, currentValue);
		try{
			if(fact.get(key) == null){
				//System.out.println("key not present in fact " + fact + key);
				return false;
			}
			String keyval =  Utils.getAsString(fact.get(key));
			boolean result = op.evaluate(keyval, currentValue);
			//System.out.println(id + " " + result + " " + keyval + " " + currentValue) ;
			return result;
		}
		catch(Exception e){
			//System.out.println(e);
		}
		return false;
	}
	
}
