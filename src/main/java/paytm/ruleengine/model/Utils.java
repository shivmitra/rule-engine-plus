package paytm.ruleengine.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;

public class Utils {
	public static String replaceVariables(JsonObject fact, String value){
		Pattern pattern = Pattern.compile("<%=(.*?)%>");
		Matcher match = pattern.matcher(value);
		int place = 1;
		while(match.find()){
			String val = match.group(place);
			try {
				JsonElement keyval = fact.get(val.trim());
				String data = "1";
				
				if(keyval != null)
					data = Utils.getAsString(keyval);
				
				value = value.replaceFirst("<%="+val+"%>",data);	
			} catch (Exception e){
				System.out.println(e);
				return value;
			}
		}
		return value;
	}
	
	public static String deleteVariables(String value){
		Pattern pattern = Pattern.compile("<%=(.*?)%>");
		Matcher match = pattern.matcher(value);
		int place = 1;
		while(match.find()){
			String val = match.group(place);
			value = value.replaceFirst("<%="+val+"%>"," ");	
		}
		return value;
	}
	
	public static String getAsString(JsonElement elem){
		if(elem instanceof JsonNull)
			return "null";
		return elem.getAsString();
	}
}
