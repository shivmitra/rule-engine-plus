package paytm.ruleengine.model;

import com.google.gson.JsonObject;
import paytm.ruleengine.model.interfaces.Action;

public class SetVariableAction implements Action {

	private String key;
	private String value;
	
	public SetVariableAction(String key, String value){
		this.key = key;
		this.value = value;
	}
	
	@Override
	public void execute(JsonObject fact) {
		String currentValue = Utils.replaceVariables(fact, value);
		try{
			fact.addProperty(key, currentValue);
        }catch(Exception e){
        	System.out.println(e);
        }
	}
}
