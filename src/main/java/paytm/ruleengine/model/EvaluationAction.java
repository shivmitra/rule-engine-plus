package paytm.ruleengine.model;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import com.google.gson.JsonObject;

import paytm.ruleengine.model.interfaces.Action;

public class EvaluationAction implements Action{
	private String key;
	private String value;
	private static ScriptEngine engine;
	
	public static void injectEngine(ScriptEngine injectedengine){
		engine = injectedengine;
	}
	
	public EvaluationAction(String key, String value){
		this.key = key;
		this.value = value;
		if(engine == null)
			engine = new ScriptEngineManager().getEngineByName("javascript");
	}
	
	@Override
	public void execute(JsonObject fact) {
		String currentValue = Utils.replaceVariables(fact,value);
		try{
        	currentValue = evaluateValueAsScript(fact, currentValue);
        	fact.addProperty(key, currentValue);
        }catch(Exception e){
        	System.out.println(e);
        }
	}
	
	private String evaluateValueAsScript(JsonObject fact, String value) throws ScriptException{	
		return engine.eval(value).toString();
	}
}
