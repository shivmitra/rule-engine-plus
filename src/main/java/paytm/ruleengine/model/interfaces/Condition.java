package paytm.ruleengine.model.interfaces;

import com.google.gson.JsonObject;

public interface Condition {
	public boolean evaluate(JsonObject fact);
}
