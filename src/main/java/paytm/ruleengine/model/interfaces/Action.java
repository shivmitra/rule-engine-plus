package paytm.ruleengine.model.interfaces;

import com.google.gson.JsonObject;

public interface Action {
	public void execute(JsonObject fact);
}
