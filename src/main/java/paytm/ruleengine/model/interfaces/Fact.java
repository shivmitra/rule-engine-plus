package paytm.ruleengine.model.interfaces;


public interface Fact {
	public Object getValue(String key);
	public Object setValue(String key, Object value);
}
