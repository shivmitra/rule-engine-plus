package paytm.ruleengine.model.interfaces;

public interface Operator {
	public boolean evaluate(String o1, String o2);
}
