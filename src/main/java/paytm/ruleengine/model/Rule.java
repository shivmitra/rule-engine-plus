package paytm.ruleengine.model;

import java.util.List;
import org.easyrules.core.BasicRule;

import com.google.gson.JsonObject;

import paytm.ruleengine.model.interfaces.Action;
import paytm.ruleengine.model.interfaces.Condition;


public class Rule extends BasicRule{

	private final List<Condition> conditions;
	private final List<Action> actions;
	private JsonObject fact;
	private final String conditions_op;
	
	public Rule(String name, List<Condition> conditions, List<Action> actions, 
			int priority, String conditions_op, JsonObject fact){
		
		this.setPriority(priority);
		this.conditions = conditions;
		this.actions = actions;
		this.name = name;
		this.conditions_op = conditions_op;
		this.fact = fact;
	}
	
	public void setFact(JsonObject fact){
		this.fact = fact;
	}
	
	@Override
	public boolean evaluate(){
		if(fact == null) throw new NullPointerException();
		
		if(conditions_op.equals("&&"))
			return conditions.stream()
					.map(cond -> cond.evaluate(fact))
					.reduce(true, (acc, val) -> acc & val);
		else if(conditions_op.equals("||"))
			return conditions.stream()
				.map(cond -> cond.evaluate(fact))
				.reduce(false, (acc, val) -> acc || val);
		else 
			return false;
	}
	
	@Override
	public void execute() throws Exception{
		if(fact == null) throw new NullPointerException();
		actions.forEach( action -> action.execute(fact) );
	}
}
