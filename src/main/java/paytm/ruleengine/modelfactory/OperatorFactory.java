package paytm.ruleengine.modelfactory;

import java.util.List;
import java.util.regex.Pattern;

import paytm.ruleengine.model.interfaces.Operator;
import paytm.ruleengine.modelfactory.OperatorUtils.Range;

public class OperatorFactory {
	public static final boolean DEFAULT_RETURN = true;

	public static Operator getOperator(String type){
		
		// in case we don't have operator, we will pass it as default
		Operator op = (a,b)->DEFAULT_RETURN;
		
		switch(type){
			case "=":
				op = (a,b) -> {
					if(OperatorUtils.getType(a) == Integer.class)	
						return OperatorUtils.compareAsInt(a,b) == 0;
					return a.compareTo(b) == 0;
				};
				break;
			case "!=":
				op = (a,b) -> {
					if(OperatorUtils.getType(a) == Integer.class)	
						return OperatorUtils.compareAsInt(a,b) != 0;
					return a.compareTo(b) != 0;
				};
				break;
			case "<":
				op = (a,b) -> {
					if(OperatorUtils.getType(a) == Integer.class)	
						return OperatorUtils.compareAsInt(a,b) < 0;
					return a.compareTo(b) < 0;
				};
				break;
			case ">":
				op = (a,b) -> {
					if(OperatorUtils.getType(a) == Integer.class)	
						return OperatorUtils.compareAsInt(a,b) > 0;
					return a.compareTo(b) > 0;
				};
				break;
			case ">=":
				op = (a,b) -> {
					if(OperatorUtils.getType(a) == Integer.class)	
						return OperatorUtils.compareAsInt(a,b) >= 0;
					return a.compareTo(b) >= 0;
				};
				break;
			case "<=":
				op = (a,b) -> {
					if(OperatorUtils.getType(a) == Integer.class)	
						return OperatorUtils.compareAsInt(a,b) <= 0;
					return a.compareTo(b) <= 0;
				};
				break;	
			case "set":
				op = (a,b) -> {
					return OperatorUtils.isStringInList(a,b);
				};
				break;
			case "!set":
				op = (a,b) -> {
					return !OperatorUtils.isStringInList(a,b);
				};
				break;
			case "datetimerange":
				op = (a,b) -> {
					return OperatorUtils.isDateTimeInRange(a, b);
				};
				break;
			case "!datetimerange":
				op = (a,b) -> {
					return !OperatorUtils.isDateTimeInRange(a, b);
				};
				break;
			case "timerange":
				op = (a,b) -> {	
					return OperatorUtils.isTimeInRange(a, b);
				};
				break;
			case "!timerange":
				op = (a,b) -> {
					return !OperatorUtils.isTimeInRange(a, b);
				};
				break;
			case "stringrange":
				op = (a,b) -> {
					return OperatorUtils.isStringInList(a, b);
				};
				break;
			case "!stringrange":
				op = (a,b) -> {
					return !OperatorUtils.isStringInList(a, b);
				};
				break;
			case "range":
				op = (a,b) -> {
					int val = Integer.parseInt(a.trim());
					List<Range> ranges = OperatorUtils.convertToRangeArray(b);
					return ranges.stream().map(range -> range.hasInt(val))
						.reduce(false, (v1,v2) -> v1 || v2);
				};
				break;
			case "!range":
				op = (a,b) -> {
					int val = Integer.parseInt(a.trim());
					List<Range> ranges = OperatorUtils.convertToRangeArray(b);
					return !(ranges.stream().map(range -> range.hasInt(val))
						.reduce(false, (v1,v2) -> v1 || v2));
				};
				break;
			case "regex":
				op = (a,b) -> {
					return Pattern.matches(b, a);
				};
				break;
			case "!regex":
				op = (a,b) -> {
					return !Pattern.matches(b, a);
				};
				break;
		}
		return op;
	}
	
}
