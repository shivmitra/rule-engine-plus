package paytm.ruleengine.modelfactory;

import paytm.ruleengine.model.CheckVariableCondition;
import paytm.ruleengine.model.interfaces.Condition;
import paytm.ruleengine.model.interfaces.Operator;

public class ConditionFactory {
	public static Condition getCondition(String type, int id, String key, String ops, String value){
		Operator op = OperatorFactory.getOperator(ops);
		return new CheckVariableCondition(id,key, op, value);	
	}
}

