package paytm.ruleengine.modelfactory;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class OperatorUtils {
	
	private static String dtString = "YYYY-MM-dd HH:mm:ss";
	private static String tString = "HH:mm:ss";
	private static DateTimeFormatter dtformat = DateTimeFormat.forPattern(dtString); 
	private static DateTimeFormatter tformat = DateTimeFormat.forPattern(tString); 
	private static ConcurrentHashMap<String, HashSet<String> > setCache = new ConcurrentHashMap<>();
	
	public static class Range{
		public Range(int a, int b){
			this.upper = Math.max(a,b);
			this.lower = Math.min(a,b);
		}
		private int upper;
		private int lower;
		
		public boolean hasInt(int b){
			return b >= lower && b <=upper;
		}
	}
	
	public static List<Range> convertToRangeArray(String b){
		String[] list = b.split(",");
		return Arrays.asList(list).stream()
				.map(str -> {
					String[] range = str.split("~",-1);
					
					if(range.length == 1){
						return new Range(Integer.parseInt(range[0].trim()),Integer.parseInt(range[0].trim()));
					}else{
						if("".equals(range[0]))
							return new Range(Integer.MIN_VALUE,Integer.parseInt(range[1].trim()));
						else if("".equals(range[1]))
							return new Range(Integer.parseInt(range[0].trim()),Integer.MAX_VALUE);
						else
							return new Range(Integer.parseInt(range[0].trim()), Integer.parseInt(range[1].trim()));
					}
				}).collect(Collectors.toList());
	}

	public static boolean isIntegerInList(String a, String b){
		Integer ia = Integer.parseInt(a.trim());
		String[] list = b.split(",");
		List<Integer> values = Arrays.asList(list).stream().map(Integer::parseInt)
							.collect(Collectors.toList());
		return values.contains(ia);
	}
	
	// Uses cache of sets for optimization. 
	public static boolean isStringInList(String a, String b){
		String[] acceptedList = b.split(",");
		HashSet<String> acceptedSet;
		if(!setCache.contains(b)){
			acceptedSet = new HashSet<>();
			acceptedSet.addAll(Arrays.asList(acceptedList));
			setCache.put(b, acceptedSet);
		}else{
			acceptedSet = setCache.get(b);
		}
		
		HashSet<String> factSet = new HashSet<>();
		factSet.addAll(Arrays.asList(a.split(",")));
		
		return containsAny(factSet,acceptedSet);
	}
	
	public static boolean containsAny(HashSet<String> factSet, HashSet<String> acceptedSet){
		for(String s:factSet){
			if(acceptedSet.contains(s))
				return true;
		}
		
		return false;
	}
	
	public static int compareAsInt(String a, String b){
		Integer ia = Integer.parseInt(a.trim());
		Integer ib = Integer.parseInt(b.trim());
		return ia.compareTo(ib);
	}
	
	public static Type getType(String value){
		if(isInt(value))
			return Integer.class;
		return String.class;
	}
	
	private static boolean isInt(String value){
		try{
			Integer.parseInt(value);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public static boolean isDateTimeInRange(String a, String b){
		if(!isValidDateTime(a))
			return false;
		String[] arr = b.split("~");
		if(!isValidDateTime(arr[0]) || !isValidDateTime(arr[1]))
			return false;
		try{
			DateTime ruletimestart = dtformat.parseDateTime(arr[0].trim());
			DateTime ruletimeend = dtformat.parseDateTime(arr[1].trim());
			DateTime curtime = dtformat.parseDateTime(a.trim());
			return curtime.isAfter(ruletimestart) && curtime.isBefore(ruletimeend);
		}catch(Exception e){
			return false;
		}
	}
	
	private static boolean isValidDateTime(String v){
		return v.trim().length() == dtString.length();
	}
	
	private static boolean isValidTime(String t){
		return t.trim().length() == tString.length();
	}
	
	public static boolean isTimeInRange(String a, String b){
		if(!isValidTime(a))
			return false;
			
		String[] arr = b.split("~");
		
		if(!isValidTime(arr[0]) || !isValidTime(arr[1]))
			return false;
		
		try{
			DateTime ruletimestart = tformat.parseDateTime(arr[0].trim());
			DateTime ruletimeend = tformat.parseDateTime(arr[1].trim());
			DateTime curtime = tformat.parseDateTime(a.trim());
			return curtime.isAfter(ruletimestart) && curtime.isBefore(ruletimeend);
		}catch(Exception e){
			return false;
		}
	}
}
