package paytm.ruleengine.modelfactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.gson.JsonObject;

import paytm.rule.model.RuleModel;
import paytm.ruleengine.RuleEngine;

public class RuleEngineFactory {
	private Map<String, RuleModel[] > tagToRulesMap = new HashMap<>();
	private Map<String,RuleEngine> tagToEngineMap = new HashMap<>();
	
	public RuleEngineFactory(RuleModel[] rulemodels){
		Set<String> tags = new HashSet<>();
		Arrays.asList(rulemodels).stream().filter(rulemodel -> Objects.nonNull(rulemodel))
			.forEach(model -> {
			Arrays.asList(model.tags).stream().filter(tag -> Objects.nonNull(tag))
			.forEach( tag -> {
				tags.add(tag);
			});
		});
		
		// null tag for all rules to fire irrespctive of tag
		tags.add(null);
		
		tags.forEach(tag -> {
			List<RuleModel> rules = Arrays.asList(rulemodels).stream()
				.filter(rulemodel -> Objects.nonNull(rulemodel))
				.filter(rulemodel -> {
					if(tag == null)
						return true;
					return Arrays.asList(rulemodel.tags).contains(tag);
				})
				.collect(Collectors.toList());
			
			tagToRulesMap.put(tag, rules.toArray(new RuleModel[rules.size()]));
		});
				
	}
	
	public RuleEngine getRuleEngine(String tag, JsonObject jsonObject){
		if(tagToRulesMap.containsKey(tag)){
			tagToEngineMap.put(tag,new RuleEngine(tagToRulesMap.get(tag), jsonObject));
			return tagToEngineMap.get(tag);
		}
		
		throw new RuntimeException("no rule Engine for tag " + tag);
	}
	
	public RuleEngine getCachedRuleEngine(String tag, JsonObject jsonObject){
		if(tagToEngineMap.containsKey(tag))
			tagToEngineMap.get(tag).setFact(jsonObject);
		else 
			tagToEngineMap.put(tag,getRuleEngine(tag, jsonObject));
		
		return tagToEngineMap.get(tag);
	}
}