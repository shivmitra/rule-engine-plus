package paytm.ruleengine.modelfactory;

import paytm.ruleengine.model.EvaluationAction;
import paytm.ruleengine.model.ExitAction;
import paytm.ruleengine.model.SetVariableAction;
import paytm.ruleengine.model.interfaces.Action;

public class ActionFactory {
	public static Action getAction(String type, String key, String value){
		switch(type){
			case "DANGEROUS_EVAL":
				return new EvaluationAction(key, value);
			case "RE_EXIT":
				return new ExitAction();
		}
		return new SetVariableAction(key, value);
	}
}
