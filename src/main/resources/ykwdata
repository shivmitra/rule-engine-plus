  [
            // Cases
            {
                'testCaseName'  : '#1 Should should pass a natural number check for a positive integer',

                'tag_to_exec'   : 'natural',
                'rules_executed' : 2,

                'message'       : {
                                    "integer"   : 1,
                                    "string"    : "abcdef",
                                    "time"      : "13:24:30",
                                    "datetime"  : "2015-01-01 00:00:00"
                                },

                'output'        : { 'integer': 1, 'string': 'abcdef', 'time': '13:24:30', 'datetime': '2015-01-01 00:00:00', 'is_natural': 1, 'eval_val' : 5 }
            },
            {
                'testCaseName'  : '#2 Should not pass a natural number check for 0',
                'rules_executed' : 10,

                'message'       : {
                                    "integer" : 0,
                                    "datetime_wrong" : "asdf 23:42:#35:wrong_date",
                                    "time_wrong"     : "asdf 23:42:#35:wrong_date"
                                },

                'output'        : {
                    "integer": 0,
                    "is_weird": 1,
                    "template_eval_val": 1,
                    "set_variable_eval": "0",
                    'wrong_cond' : 1,
                    'true': true,
                    'false': false,
                    'null': null,
                    'wrong' : true,

                    "datetime_wrong" : "asdf 23:42:#35:wrong_date",
                    "time_wrong"     : "asdf 23:42:#35:wrong_date"
                }

            },
            {
                'testCaseName'  : '#3 Should not pass a natural number check for negative numbers',
                'rules_executed' : 10,

                'message'       : {
                                    "integer" : -1,
                                    "datetime_wrong" : "asdf 23:42:#35:wrong_date",
                                    "time_wrong"     : "asdf 23:42:#35:wrong_date"
                                },

                'output'        : {
                    "integer": -1,
                    "is_weird": 1,
                    "eval_val": 2,
                    "template_eval_val": 0,
                    "set_variable_eval": "-1",
                    'wrong_cond' : 1,
                    'true': true,
                    'false': false,
                    'null': null,

                    'wrong' : true,

                    "datetime_wrong" : "asdf 23:42:#35:wrong_date",
                    "time_wrong"     : "asdf 23:42:#35:wrong_date"
                }
                
            },

            {
                'testCaseName'  : '#4 Checking all ANTI conditions in OR rule',
                'tag_to_exec'   : 'or_rule',
                'rules_executed': 1,

                'message'       : {
                                    "integer"   : 1,
                                    "string"    : "abcdef",
                                    "time"      : "13:24:30",
                                    "datetime"  : "2015-01-01 00:00:00"
                                },

                'output'        : { 'integer': 1, 'string': 'abcdef', 'time': '13:24:30', 'datetime': '2015-01-01 00:00:00', 'is_weird' : 1 }
            },

            {
                'testCaseName'  : '#5 Checking template condition and complex conditions',
                'tag_to_exec'   : 'template_condition',
                'rules_executed': 1,

                'message'       : {
                                    "integer"   : 1,
                                    "string"    : "abcdef",
                                    "time"      : "13:24:30",
                                    "datetime"  : "2015-01-01 00:00:00"
                                },

                'output'        : { 'integer': 1, 'string': 'abcdef', 'time': '13:24:30', 'datetime': '2015-01-01 00:00:00', 'eval_val' : 2 }
            },

            {
                'testCaseName'  : '#6 Template Eval',
                'tag_to_exec'   : 'template_eval',
                'rules_executed': 1,

                'message'       : {
                                    "integer"   : 1,
                                    "string"    : "abcdef",
                                    "time"      : "13:24:30",
                                    "datetime"  : "2015-01-01 00:00:00"
                                },

                'output'        : { 'integer': 1, 'string': 'abcdef', 'time': '13:24:30', 'datetime': '2015-01-01 00:00:00', 'template_eval_val' : 2 }
            },

            {
                'testCaseName'  : '#7 Set variable Eval',
                'tag_to_exec'   : 'template_action_eval',
                'rules_executed': 1,

                'message'       : {
                                    "integer"   : 1,
                                    "string"    : "abcdef",
                                    "time"      : "13:24:30",
                                    "datetime"  : "2015-01-01 00:00:00"
                                },

                'output'        : { 'integer': 1, 'string': 'abcdef', 'time': '13:24:30', 'datetime': '2015-01-01 00:00:00', 'set_variable_eval' : '1' }
            },

            {
                'testCaseName'  : '#8 wrong condition test case',
                'tag_to_exec'   : 'wrong_cond',
                'rules_executed': 1,

                'message'       : {
                                    "integer"   : 1,
                                    "string"    : "abcdef",
                                    "time"      : "13:24:30",
                                    "datetime"  : "2015-01-01 00:00:00"
                                },

                'output'        : { 'integer': 1, 'string': 'abcdef', 'time': '13:24:30', 'datetime': '2015-01-01 00:00:00', 'wrong_cond' : 1 }
            },

            {
                'testCaseName'  : '#9 Wrong datetime format',
                'tag_to_exec'   : 'wrong_datetime',
                'rules_executed': 1,

                'message'       : {
                                    "integer"   : 1,
                                    "string"    : "abcdef",
                                    "time"      : "13:24:30",
                                    "datetime"  : "2015-01-01 00:00:00",
                                    "datetime_wrong" : "asdf",
                                    "time_wrong"    : "asdf"
                                },
                'output'        : {
                                    "integer"   : 1,
                                    "string"    : "abcdef",
                                    "time"      : "13:24:30",
                                    "datetime"  : "2015-01-01 00:00:00",
                                    "datetime_wrong" : "asdf",
                                    "time_wrong"    : "asdf",
                                    "wrong" : true
                                }
            },
            {
                'testCaseName' : '#10 Should not pass a nature number check for numbers defined as null',
                'rules_executed' : 10,
                'message' : {
                    'integer' : null,
                    'datetime_wrong' : null,
                    'time_wrong' : null
                },

                'output' : {
                    "integer": null,
                    "is_weird": 1,
                    "template_eval_val": 1,
                    "set_variable_eval": "",
                    'wrong_cond' : 1,
                    'true': true,
                    'false': false,
                    'null': null,
                    'wrong' : true,
                    "datetime_wrong" : null,
                    "time_wrong"     : null
                }
            },

            {
                'testCaseName' : '#11 Should not ',
                'tag_to_exec'   : 'unknown_action',
                'rules_executed' : 1,
                'message' : {
                    'integer' : null,
                    'datetime_wrong' : null,
                    'time_wrong' : null
                },
                'output' : {
                    'integer' : null,
                    'datetime_wrong' : null,
                    'time_wrong' : null
                }
            }


        ]